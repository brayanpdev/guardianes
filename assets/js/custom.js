
/*
Use this file to write your custom javascript code
*/

const formGuardianes= document.getElementById('form-guardianes');

formGuardianes.addEventListener('submit',function (e) {
	/* body... */
	e.preventDefault();

            const xhr= new XMLHttpRequest();
            const formData= new FormData();
            formData.append('raza',document.getElementById('raza').value);
            formData.append('nombre',document.getElementById('nombre').value);
            formData.append('edad',document.getElementById('edad').value);
            formData.append('whatsapp',document.getElementById('whatsapp').value);

            //xhr.open('POST','https://guardianes.com/enviarEmail.php',true);
            xhr.open('POST','http://donhosting.com.ve/guardianes/guardianes/enviarEmail.php',true);
            xhr.onload= function () {
                if (this.status===200) {
                    const res= JSON.parse(this.responseText);

                    //se muestra el mensaje en caso de error
                    if (res==true) {
                        const divMensaje= document.getElementById('mensajeForm');
                        divMensaje.innerHTML='<div class="col-lg-12"><div class="alert alert-danger" role="alert">Disculpe no pudimos enviar su correo, intente mas tarde.+'
                                             '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div></div>'

                    }else{
                        const divMensaje= document.getElementById('mensajeForm');
                        divMensaje.innerHTML='<div class="col-lg-12"><div class="alert alert-success" role="alert">Gracias por escribir pronto nos pondremos en contacto con usted.'+
                                             '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></div></div>';

                    }
                    
                    
                }else{
                    console.log(this);return;
                }
            }//--fin xhr onload

            xhr.send(formData);
})